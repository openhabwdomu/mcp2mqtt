from smbus import SMBus
import paho.mqtt.client as mqtt
import time
import logging

# Define registers values from MCP23017 datasheet
IODIRA = 0x00  # IO direction A - 1= input 0 = output
IODIRB = 0x01  # IO direction B - 1= input 0 = output
IPOLA = 0x02  # Input polarity A
IPOLB = 0x03  # Input polarity B
GPINTENA = 0x04  # Interrupt-onchange A
GPINTENB = 0x05  # Interrupt-onchange B
DEFVALA = 0x06  # Default value for port A
DEFVALB = 0x07  # Default value for port B
INTCONA = 0x08  # Interrupt control register for port A
INTCONB = 0x09  # Interrupt control register for port B
IOCON = 0x0A  # Configuration register
GPPUA = 0x0C  # Pull-up resistors for port A
GPPUB = 0x0D  # Pull-up resistors for port B
INTFA = 0x0E  # Interrupt condition for port A
INTFB = 0x0F  # Interrupt condition for port B
INTCAPA = 0x10  # Interrupt capture for port A
INTCAPB = 0x11  # Interrupt capture for port B
GPIOA = 0x12  # Data port A
GPIOB = 0x13  # Data port B
OLATA = 0x14  # Output latches A
OLATB = 0x15  # Output latches B

logging.basicConfig(level=logging.DEBUG, format='   %(asctime)s %(levelname)-8s %(message)s')
logger = logging.getLogger()

# =================== Configuration ===============================
ic2_out_address = 0x20  # Address of output  MCP23017
ic2_in_address = 0x21   # Address of input MCP23017
bus_no = 1
mqtt_server_ip = "localhost"
mqtt_servert_port = 1883
mqtt_username = "openhabian"
mqtt_password = "openhabian"
# ==================================================================


i2cbus = SMBus(bus_no)  # Create a new I2C bus

# Set pin direction first MCP
i2cbus.write_byte_data(ic2_in_address, IODIRA, 0xff)  # All pins input
i2cbus.write_byte_data(ic2_in_address, IODIRB, 0xff)  # All pins input

# Set pin direction second  MCP
i2cbus.write_byte_data(ic2_out_address, IODIRA, 0x00)  # All pins output
i2cbus.write_byte_data(ic2_out_address, IODIRB, 0x00)  # All pins output

def on_connect(client, userdata, flags, rc, properties):
    """ Connection MQTT handler"""
    if rc.is_failure:
        logger.info("Failed to connect: " + str(rc) + " loop_forever() will retry connection")
    else:
        client.subscribe([
            ("mcp23017/20/A/output/0",0),("mcp23017/20/A/output/1",0),("mcp23017/20/A/output/2",0),
            ("mcp23017/20/A/output/3",0),("mcp23017/20/A/output/4",0),("mcp23017/20/A/output/5",0),
            ("mcp23017/20/A/output/6",0),("mcp23017/20/A/output/7",0),
            ("mcp23017/20/B/output/0",0),("mcp23017/20/B/output/1",0),("mcp23017/20/B/output/2",0),
            ("mcp23017/20/B/output/3",0),("mcp23017/20/B/output/4",0),("mcp23017/20/B/output/5",0),
            ("mcp23017/20/B/output/6",0),("mcp23017/20/B/output/7",0),])
        logger.info("Subscribed to all topics")

def on_message(client, userdata, msg):
    """ Handling incoming MQTT message"""

    module = msg.topic[12]
    out_pin = int(msg.topic[-1])
    mcp_mask = 1 << out_pin
    mcp_mask_inv = mcp_mask^0xff
    value = msg.payload.decode()

    prev_val = 0;
    GPIOx    = GPIOA
    OLATx    = OLATA
    if module == "A":
        GPIOx = GPIOA
        OLATx = OLATA
    else:
        GPIOx = GPIOB
        OLATx = OLATB

    # read what is current output value
    prev_val = i2cbus.read_byte_data(ic2_out_address, GPIOx)

    if value == "ON":
        # clear bit on out_pin position
        olatx_val = prev_val & mcp_mask_inv
        i2cbus.write_byte_data(ic2_out_address, OLATx, olatx_val)

    else:
        # set bit on out_pin position
        olatx_val = prev_val | mcp_mask
        i2cbus.write_byte_data(ic2_out_address, OLATx, prev_val | olatx_val)

    logger.info("Output " + msg.topic + " " + str(value) + ": OLAT" + module + "<=" + bin(olatx_val))


def find_bit_diff(prev_val, cur_val):
    """ Generate what bit in byte was changed between new and old values and return list of key/value """
    diff_arr = []
    # check if cometing changed in value
    if cur_val != prev_val:
        # check of following bites
        pos = 0
        xor_val = cur_val ^ prev_val  # check where are differencec, 1 - there is a difference
        for mask in (0x1, 0x2, 0x4, 0x8, 0x10, 0x20, 0x40, 0x80):
            if xor_val & mask:  # was bit changed?
                inp_bit = "OPEN" if cur_val & mask == 0 else "CLOSED"
                diff_arr.append({"pos": pos, "val": inp_bit})
            pos = pos + 1
    return diff_arr


def main():
    logger.info(" ==== Starting mcp2mqtt 1.0 (Michal Szymanski michal@szymanskich.net) === ")

    # MQTT configuration
    client = mqtt.Client(mqtt.CallbackAPIVersion.VERSION2)
    logger.info("Using user[" + mqtt_username + "] to connect to MQTT broker")
    client.username_pw_set(username=mqtt_username, password=mqtt_password)
    client.connect(mqtt_server_ip, mqtt_servert_port, 60)

    client.on_connect = on_connect
    client.on_message = on_message
    client.loop_start()

    prev_value_a = 0
    prev_value_b = 0

    first_iteration = True

    while True:
        time.sleep(0.2)

        inp_data_a = i2cbus.read_byte_data(ic2_in_address, GPIOA)
        inp_data_b = i2cbus.read_byte_data(ic2_in_address, GPIOB)

        if first_iteration == True:
            # First read from MCP send all information to the topics
            prev_value_a = inp_data_a ^ 0xff
            prev_value_b = inp_data_b ^ 0xff
            first_iteration = False

        val_to_send = find_bit_diff(prev_val=prev_value_a, cur_val=inp_data_a)
        prev_value_a = inp_data_a
        for element in val_to_send:
            logger.info("Input mcp23017/21/A/input/" + str(element["pos"]) + " " + element["val"])
            client.publish("mcp23017/21/A/input/" + str(element["pos"]), element["val"])

        val_to_send = find_bit_diff(prev_val=prev_value_b, cur_val=inp_data_b)
        prev_value_b = inp_data_b
        for element in val_to_send:
            logger.info("Input mcp23017/21/B/input/" + str(element["pos"]) + " " + element["val"])
            client.publish("mcp23017/21/B/input/" + str(element["pos"]), element["val"])

if __name__ == "__main__":
    main()
