# Preryquisite
Make sure that you activeted I2C https://ozzmaker.com/i2c/
# Installation
    cd /home/openhabian
    git clone https://gitlab.com/openhabwdomu/mcp2mqtt.git
    cd mcp2mqtt
    python3 -m venv env 
    source env/bin/activate
    pip install smbus
    pip install paho-mqtt
    sudo mkdir /var/log/mcp2mqtt   
    sudo cp mcp2mqtt.service /etc/systemd/system
# Starting / stopping
    sudo systemctl start mcp2mqtt
    sudo systemctl stop mcp2mqtt
# Logs (directiory must be created earlier)
    tail -f /var/log/mcp2mqtt/mcp2mqtt.log
# Make automatic start after reboot
  sudo systemctl enable mcp2mqtt
# Documentation:
 https://medium.com/@benmorel/creating-a-linux-service-with-systemd-611b5c8b91d6
